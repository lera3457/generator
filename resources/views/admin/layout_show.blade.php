@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Макет</div>
                    <div class="card-body">
                        <h4>{{$layout->name}} за {{$layout->year}} год состоит из разделов:</h4>
                        @if(count($sections) > 0)
                           <div style="padding: 36px 110px;">
                            @foreach($sections as $section)
                               {!! $section !!}
                            @endforeach
                           </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
