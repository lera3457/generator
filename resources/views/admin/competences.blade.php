@extends('layouts.app')

@section('content')
    <div class="container" style="max-width: 1600px;">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Рабочие программы</div>

                    <div class="card-body">
                        <div class="btn-group" style="float: right;">
                            <a href="{{ route('competences.create') }}" class=" btn btn-default">
                                Добавить компетенции по новому направлению
                            </a>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Направление подготовки</th>
                                <th style="text-align: right;">Функционал</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($competences->count())
                                @foreach($competences as $competence)
                                    <tr>
                                        <td>{{ $competence->directions_name }}</td>
                                        <td style="text-align: right;">
                                            <a href="{{ route('competences.show', $competence->id) }}" class="btn btn-primary">Просмотр</a>&nbsp; &nbsp;
                                            <a href="{{ route('competences.edit', $competence->id) }}" class="btn btn-success">Редактирование</a>&nbsp; &nbsp;
                                            <a onclick="event.preventDefault(); document.getElementById('delete-product_{{$competence->id}}').submit();" href='#' class="btn btn-danger">Удаление</a>
                                            <form id="delete-product_{{$competence->id}}"
                                                  action="{{ route('competences.destroy', ['id' => $competence->id]) }}"
                                                  method="POST"
                                                  style="display: none;"
                                            >
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
