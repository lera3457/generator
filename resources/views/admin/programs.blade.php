@extends('layouts.app')

@section('content')
    <div class="container" style="max-width: 1600px;">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Рабочие программы</div>

                    <div class="card-body">
                        <div class="btn-group" style="float: right;">
                            <a href="{{ route('programs.create') }}" class=" btn btn-default">
                                Добавить программу
                            </a>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Направление подготовки</th>
                                <th>Год</th>
                                {{--<th>Исполнитель</th>--}}
                                <th style="text-align: right;">Функционал</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($programs->count())
                                @foreach($programs as $program)
                                    <tr>
                                        <td>{{ $program->name }}</td>
                                        <td>{{ $program->directions->name }}</td>
                                        <td>{{ $program->year }}</td>
                                        <td style="text-align: right;">
                                            <a href="{{ route('programs.show', $program->id) }}" class="btn btn-primary">Просмотр</a>&nbsp; &nbsp;
                                            <a href="{{ route('programs.edit', $program->id) }}" class="btn btn-success">Редактирование</a>&nbsp; &nbsp;
                                            <a onclick="event.preventDefault(); document.getElementById('delete-product_{{$program->id}}').submit();" href='#' class="btn btn-danger">Удаление</a>
                                            <form id="delete-product_{{$program->id}}"
                                                  action="{{ route('programs.destroy', ['id' => $program->id]) }}"
                                                  method="POST"
                                                  style="display: none;"
                                            >
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
