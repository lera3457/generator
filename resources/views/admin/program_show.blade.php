@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Рабочая программа</div>
                    <div class="card-body">
                        <h4>{{$layout->name}}</h4>
                        @if(count($sections) > 0)
                        <div class="form-group">
                            <div class="col-lg-6">
                                <label for="text" class="form-control-label">Содержание: </label>
                            </div>
                            <div class="col-lg-12 text"><!--общую сумму часов для каждой работы в отдельности -->
                                <textarea id='mytextarea' class="form-control" name="text" id="text" rows="10">
                                @foreach($sections as $section)
                                        {!! $section !!}
                                    @endforeach
                                </textarea>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
