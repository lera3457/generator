@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Компетенции по направлению «{{$direction->name}}»</div>
                    <div class="card-body">
                        <div class="col-lg-12">
                            <div class="form-group" id="competence_op">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Общекультурные компетенции (ОК)
                                        </div>
                                        <div id="accordion">
                                            @if(count($competences) > 0)
                                                @foreach($competences as $competence)
                                                    @if($competence->type_id == 1)
                                                        <div class="card">
                                                            <div class="card-header"
                                                                 id="heading_{{$competence->id}}">
                                                                <h5 class="mb-0">
                                                                    <a style="color: #3490dc" class="btn btn-link"
                                                                       data-toggle="collapse"
                                                                       data-target="#collapse{{$competence->id}}"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapse{{$competence->id}}"
                                                                       role="button">{{$competence->name}}</a>
                                                                </h5>
                                                            </div>
                                                            <div id="collapse{{$competence->id}}"
                                                                 class="collapse"
                                                                 aria-labelledby="heading{{$competence->id}}"
                                                                 data-parent="#accordion" style="">
                                                                <div class="card-body">
                                                                    {{$competence->content}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="competence_opk">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Общепрофессиональные компетенции (ОПК)
                                        </div>
                                        <div id="accordion">
                                            @if(count($competences) > 0)
                                                @foreach($competences as $competence)
                                                    @if($competence->type_id == 2)
                                                        <div class="card">
                                                            <div class="card-header"
                                                                 id="heading_{{$competence->id}}">
                                                                <h5 class="mb-0">
                                                                    <a style="color: #3490dc" class="btn btn-link"
                                                                       data-toggle="collapse"
                                                                       data-target="#collapse{{$competence->id}}"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapse{{$competence->id}}"
                                                                       role="button">{{$competence->name}}</a>
                                                                </h5>
                                                            </div>
                                                            <div id="collapse{{$competence->id}}"
                                                                 class="collapse"
                                                                 aria-labelledby="heading{{$competence->id}}"
                                                                 data-parent="#accordion" style="">
                                                                <div class="card-body">
                                                                    {{$competence->content}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="competence_pk">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Профессиональные компетенции (ПК)
                                        </div>
                                        <div id="accordion">
                                            @if(count($competences) > 0)
                                                @foreach($competences as $competence)
                                                    @if($competence->type_id == 3)
                                                        <div class="card">
                                                            <div class="card-header"
                                                                 id="heading_{{$competence->id}}">
                                                                <h5 class="mb-0">
                                                                    <a style="color: #3490dc" class="btn btn-link"
                                                                       data-toggle="collapse"
                                                                       data-target="#collapse{{$competence->id}}"
                                                                       aria-expanded="false"
                                                                       aria-controls="collapse{{$competence->id}}"
                                                                       role="button">{{$competence->name}}</a>
                                                                </h5>
                                                            </div>
                                                            <div id="collapse{{$competence->id}}"
                                                                 class="collapse"
                                                                 aria-labelledby="heading{{$competence->id}}"
                                                                 data-parent="#accordion" style="">
                                                                <div class="card-body">
                                                                    {{$competence->content}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
