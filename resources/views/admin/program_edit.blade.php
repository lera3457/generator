@extends('layouts.app')

@section('content')
    <div class="container" style="max-width: 1523px;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$section_first->name}}</div>

                    <div class="card-body" id="huehf">
                        @if((strpos($section_first->name, 'СТРУКТУРА И СОДЕРЖАНИЕ ДИСЦИПЛИНЫ') !== false) &&
                        (strpos($section_first_value->description_value, '#gk_workProg#') !== false || strpos($section_first_value->description_value, '#ik_workProg#') !== false ||
                        strpos($section_first_value->description_value, '#ai_workProg#') !== false))
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Зачетные единицы по данной дисциплине: </label>
                                </div>
                                <div class="col-lg-6 text">
                                    <input type="text" id="ball" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="gk" class="form-control-label">Групповые консультации (ГК): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="gk" name="gk" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="ik" class="form-control-label">Индивидуальные консультации (ИК): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="ik" name="ik" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="ai" class="form-control-label">Аттестационные испытания (АИ): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="ai" name="ai" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="l_09" class="form-control-label">Занятия лекционного типа (Л): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="l_09" name="l_09" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="pz" class="form-control-label">Практические занятия (ПЗ): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="pz" name="pz" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="lr" class="form-control-label">Лабораторные работы (ЛР): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="lr" name="lr" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4">
                                    <div class="col-lg-12">
                                        <label for="sr" class="form-control-label">Самостоятельная работа (СР): </label>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <input type="text" id="sr" name="sr" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <a style="color:white" class="btn btn-primary" id="time_equals" data-id="{{$section_first->id}}" data-prog_id="{{$program->id}}">Продолжить</a>
                                </div>
                            </div>
                        @elseif(strpos($section_first->name, 'УЧЕБНО-МЕТОДИЧЕСКОЕ И ИНФОРМАЦИОННОЕ') !== false)
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Вид литературы: </label>
                                </div>
                                <select class="form-control col-lg-6" id="class_lit" style="margin: 0 15px" >
                                    <option value="0">Основная литература</option>
                                    <option value="1">Дополнительная литература</option>
                                    <option value="2">Ресурсы информационной сети</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-8">
                                    <label for="ball" class="form-control-label">Наименование: </label>
                                </div>
                                <div class="col-lg-8 text">
                                    <input type="text" id="name_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group lit_book">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Авторы: </label>
                                </div>
                                <div class="col-lg-6 text">
                                    <input type="text" id="author_lit" name="ball" class="form-control" required placeholder="Укажите авторов через запятую">
                                </div>
                            </div>
                            <div class="form-group lit_book">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Издательство: </label>
                                </div>
                                <div class="col-lg-6 text">
                                    <input type="text" id="izd_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group lit_book">
                                <div class="col-lg-4">
                                    <label for="ball" class="form-control-label">Год: </label>
                                </div>
                                <div class="col-lg-4 text">
                                    <input type="text" id="year_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group lit_book">
                                <div class="col-lg-4">
                                    <label for="ball" class="form-control-label">Страницы: </label>
                                </div>
                                <div class="col-lg-4 text">
                                    <input type="text" id="page_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label for="ball" class="form-control-label">URL: </label>
                                </div>
                                <div class="col-lg-4 text">
                                    <input type="text" id="url_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <a style="color:white" class="btn btn-primary" id="section_2">Добавить</a>
                                </div>
                            </div>
                            <div class="form-actions form-group cont_type type_0">
                                <h2>Основная литература</h2>
                                <ul id="type_0" data-num="0"></ul>
                            </div>
                            <div class="form-actions form-group cont_type type_1">
                                <h2>Дополнительная литература</h2>
                                <ul id="type_1" data-num="0"></ul>
                            </div>
                            <div class="form-actions form-group cont_type type_2">
                                <h2>Ресурсы информационной сети</h2>
                                <ul id="type_2" data-num="0"></ul>
                            </div>
                            <input type="hidden" id="main_lit">
                            <input type="hidden" id="dop_lit">
                            <input type="hidden" id="web_lit">
                        @elseif((strpos($section_first->name, 'ЦЕЛИ И ЗАДАЧИ') !== false) && ((strpos($section_first_value->description_value, '#purpose#') !== false) || (strpos($section_first_value->description_value, '{#tasks#}') !== false)))
                            <div class="form-group">
                                <div class="col-lg-8">
                                    <label for="purpose" class="form-control-label">Цели: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <textarea class="form-control" name="purpose"  rows="7" id="purpose"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-8">
                                    <label for="tasks" class="form-control-label">Задачи: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <textarea class="form-control" name="tasks" rows="10" id="tasks"></textarea>
                                </div>
                            </div>
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <a style="color:white" class="btn btn-primary" id="purpose_submit" data-id="{{$section_first->id}}" data-prog_id="{{$program->id}}">Продолжить</a>
                                </div>
                            </div>
                        @elseif((strpos($section_first->name, 'МЕСТО ДИСЦИПЛИНЫ') !== false) && ((strpos($section_first_value->description_value, '#previous_discipline#') !== false) ||
                        (strpos($section_first_value->description_value, '#future_discipline#') !== false) || (strpos($section_first_value->description_value, '#knowledge_m#') !== false) ||
                        (strpos($section_first_value->description_value, '#skill_m#') !== false) || (strpos($section_first_value->description_value, '#attainment_m#') !== false)))
                            <div class="form-group">
                                <div class="col-lg-8">
                                    <label for="ball" class="form-control-label">Знания: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" name="knowledge_m" class="form-control" required id="knowledge_m">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-8">
                                    <label for="ball" class="form-control-label">Умения: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" name="skill_m" class="form-control" required id="skill_m">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-8">
                                    <label for="ball" class="form-control-label">Навыки: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" name="attainment_m" class="form-control" required id="attainment_m">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Предшедствующие учебные дисциплины: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" name="previous_discipline" class="form-control" required id="previous_discipline">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Последующие учебные дисциплины: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" name="future_discipline" class="form-control" required id="future_discipline">
                                </div>
                            </div>
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <a style="color:white" class="btn btn-primary" id="place_change" data-id="{{$section_first->id}}" data-prog_id="{{$program->id}}">Продолжить</a>
                                </div>
                            </div>
                        @elseif(strpos($section_first->name, 'КОМПЕТЕНЦИИ ОБУЧАЮЩЕГОСЯ') !== false)
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Тип компетинции: </label>
                                </div>
                                <select class="form-control col-lg-10"style="margin: 0 15px">
                                    <option value="0">ОП</option>
                                    <option value="1">ОПК</option>
                                    <option value="2">ПК</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="ball" class="form-control-label">Компетинции: </label>
                                </div>
                                <select class="form-control col-lg-10"style="margin: 0 15px">
                                    @foreach($comps as $direction)
                                        <option value="{{$direction->id}}">{{$direction->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label for="ball" class="form-control-label">Знать: </label>
                                </div>
                                <div class="col-lg-4 text">
                                    <input type="text" id="url_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label for="ball" class="form-control-label">Уметь: </label>
                                </div>
                                <div class="col-lg-4 text">
                                    <input type="text" id="url_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label for="ball" class="form-control-label">Владеть: </label>
                                </div>
                                <div class="col-lg-4 text">
                                    <input type="text" id="url_lit" name="ball" class="form-control" required>
                                </div>
                            </div>
                        @else
                        <form action="{{ route('update_second', $section_first_value->id) }}" method="post">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="name" class="form-control-label">Наименование: </label>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" id="name" name="name" class="form-control" required value="{{$section_first->name}}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="text" class="form-control-label">Содержание: </label>
                                </div>
                                <div class="col-lg-12 text"><!--общую сумму часов для каждой работы в отдельности -->
                                    <textarea id='mytextarea' class="form-control" name="text" id="text" rows="10">{!! $section_first_value->description_value !!}</textarea>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <input type="submit" value="Сохранить" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            <input type="hidden" name="id_sec" id="id_sec" value="{{$section_first->id}}">
            <div class="col-md-4">
                <div class="list-group">
                    @foreach ($sections as $section)
                        <a href="{{route('edit_sections', ['id_pr' => $program->id, 'id_sec' => $section->id])}}" id="{{$section->id}}_sec" class="list-group-item list-group-item-action">{{$section->name}}</a>
                    @endforeach
                </div>
                <div class="list-group" style="margin-top: 25px">
                    <a href="{{route('create_programs', ['id_pr' => $program->id])}}" class="list-group-item">Создать новую программу на основе этой</a>
                </div>
            </div>
        </div>
    </div>
@endsection
