@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Компетенции по новому направлению</div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="col-lg-6">
                                <label for="directions" class="form-control-label">Направление: </label>
                            </div>
                            <div class="col-lg-12">
                                <select class="form-control" id="directions" name="directions">
                                    @foreach($directions as $direction)
                                        <option value="{{$direction->id}}">{{$direction->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-actions form-group">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    <div class="form-group">
                                                        <div class="col-lg-6">
                                                            <label for="type" class="form-control-label">Тип компетенции: </label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <select class="form-control" id="type" name="type">
                                                                @foreach($types as $type)
                                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-6">
                                                            <label for="name" class="form-control-label">Наименование: </label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <input type="text" id="name" name="name" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-6">
                                                            <label for="content" class="form-control-label">Содержание: </label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <textarea id="content" class="form-control" name="content" rows="10"></textarea>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="id_edit">
                                                </li>
                                                <li class="list-group-item"><a role="button" style="color: white" class="btn btn-success" id="add_competence" data-edit="false" data-href="{{ route('competences_add') }}">Добавить</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group competence_group" id="competence_op">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header">
                                                Общекультурные компетенции (ОК)
                                            </div>
                                            <div id="accordion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group competence_group" id="competence_opk">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header">
                                                Общепрофессиональные компетенции (ОПК)
                                            </div>
                                            <div id="accordion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group competence_group" id="competence_pk">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header">
                                                Профессиональные компетенции (ПК)
                                            </div>
                                            <div id="accordion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

{{--
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <a class="btn btn-primary" href="javascript:history.back()">Вернуться к списку</a>
                                </div>
                            </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
