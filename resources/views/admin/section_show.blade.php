@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Раздел</div>

                    <div class="card-body">
                        <h2>{{$section->name}}</h2>
                        {!! $section->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
