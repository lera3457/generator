@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Новый раздел</div>

                    <div class="card-body">
                        <form action="{{ route('sections.store') }}" method="post">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="name" class="form-control-label">Наименование: </label>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" id="name" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="name" class="form-control-label">Тип раздела: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control" id="description_type" data-href="{{ route('api_change_type_description') }}" name="type">
                                        <option value="">Не выбрано</option>
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="text_description">
                                <div class="col-lg-12">
                                    <div class="card border-primary mb-3">
                                        <div class="card-body text-primary">
                                            <p class="card-text" id="description_text_type"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="text" class="form-control-label">Содержание: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <textarea id='mytextarea' class="form-control" name="text" id="text" rows="10"></textarea>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <input type="submit" value="Сохранить" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
