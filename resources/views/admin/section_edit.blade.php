@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Раздел</div>

                    <div class="card-body">
                        <form action="{{ route('sections.update', $section->id) }}" method="post">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="name" class="form-control-label">Наименование: </label>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" id="name" name="name" class="form-control" required value="{{$section->name}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="name" class="form-control-label">Тип раздела: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control" id="description_type" data-href="{{ route('api_change_type_description') }}" name="type">
                                        <option value="">Не выбрано</option>
                                        @foreach($types as $type)
                                            @if($type->id == $section->type_id)
                                                <option value="{{$type->id}}" selected>{{$type->name}}</option>
                                            @else
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($section->type)
                            <div class="form-group" id="text_description" style="display: block">
                                <div class="col-lg-12">
                                    <div class="card border-primary mb-3">
                                        <div class="card-body text-primary">
                                            <p class="card-text" id="description_text_type">{{$section->type->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                                <div class="form-group" id="text_description">
                                    <div class="col-lg-12">
                                        <div class="card border-primary mb-3">
                                            <div class="card-body text-primary">
                                                <p class="card-text" id="description_text_type"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="text" class="form-control-label">Содержание: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <textarea id='mytextarea' class="form-control" name="text" id="text" rows="10">{!! $section->description !!}</textarea>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <input type="submit" value="Сохранить" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
