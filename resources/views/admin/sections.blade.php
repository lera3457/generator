@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Разделы </div>

                    <div class="card-body">
                        <div class="btn-group" style="float: right;">
                            <a href="{{ route('sections.create') }}" class=" btn btn-default">
                                Добавить раздел
                            </a>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th style="text-align: right;">Функционал</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($sections->count())
                                @foreach($sections as $section)
                                    <tr>
                                        <td style="width:400px;">{{ $section->name }}</td>
                                        <td style="text-align: right;">
                                            <a href="{{ route('sections.show', $section->id) }}" class="btn btn-primary">Просмотр</a>&nbsp; &nbsp;
                                            <a href="{{ route('sections.edit', $section->id) }}" class="btn btn-success">Редактирование</a>&nbsp; &nbsp;
                                            <a onclick="event.preventDefault(); document.getElementById('delete-product_{{$section->id}}').submit();" href='#' class="btn btn-danger">Удаление</a>
                                            <form id="delete-product_{{$section->id}}"
                                                  action="{{ route('sections.destroy', ['id' => $section->id]) }}"
                                                  method="POST"
                                                  style="display: none;"
                                            >
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
