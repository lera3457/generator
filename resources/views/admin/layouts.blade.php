@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Макеты рабочих программ</div>

                    <div class="card-body">
                        <div class="btn-group" style="float: right;">
                            <a href="{{ route('layouts.create') }}" class=" btn btn-default">
                                Добавить макет
                            </a>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Год</th>
                                <th style="text-align: right;">Функционал</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($layouts->count())
                                @foreach($layouts as $layout)
                                    <tr>
                                        <td>{{ $layout->name }}</td>
                                        <td>{{ $layout->year }}</td>
                                        <td style="text-align: right;">
                                            <a href="{{ route('layouts.show', $layout->id) }}" class="btn btn-primary">Просмотр</a>&nbsp; &nbsp;
                                            <a href="{{ route('layouts.edit', $layout->id) }}" class="btn btn-success">Редактирование</a>&nbsp; &nbsp;
                                            <a onclick="event.preventDefault(); document.getElementById('delete-product').submit();" href='#' class="btn btn-danger">Удаление</a>
                                            <form id="delete-product"
                                                  action="{{ route('layouts.destroy', ['id' => $layout->id]) }}"
                                                  method="POST"
                                                  style="display: none;"
                                            >
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
