@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Новый макет</div>

                <div class="card-body">
                    <form action="{{ route('layouts.store') }}" method="post">
                        <div class="form-group">
                            <div class="col-lg-6">
                                <label for="name" class="form-control-label">Наименование: </label>
                            </div>
                            <div class="col-lg-12">
                                <input type="text" id="name" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group" id="context_sections">
                            <div class="col-lg-6">
                                <label for="sections" class="form-control-label">Содержание: </label>
                            </div>
                            <div class="col-lg-12">
                                <table class="col-lg-12 table-bordered table">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Раздел</th>
                                        {{--<th>Удалить</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>1</th>
                                        <th>
                                            <select class="form-control select_sections" id="sections" onchange="select_cont(this.value, 1)" name="section_1">
                                                <option value="">Выберите раздел</option>
                                                @foreach($sections as $section)
                                                    <option value="{{$section->id}}">{{$section->name}}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                    </tr>
                                    <tr id="last_tr">
                                        <th colspan="2" class="text-center">
                                            <a href="#" class="btn btn-success" id="add_section" data-num="2" data-sections="{{$sections}}">Добавить раздел</a>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="hidden" name="sections_numbers" id="section_cont">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4">
                                <label for="year" class="form-control-label">Год: </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="year" name="year" class="form-control" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="form-actions form-group">
                            <div class="col-lg-4 push-lg-4">
                                <input type="submit" value="Сохранить" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
