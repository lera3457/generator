@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Новый раздел</div>

                    <div class="card-body">
                        <form action="{{ route('programs.store') }}" method="post">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="name" class="form-control-label">Наименование дисциплины: </label>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" id="name" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="old_program" class="form-control-label">Исходная программа: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control content_direction" id="old_program" name="old_program">
                                        <option value="">Не выбрано</option>
                                        @foreach($programs as $program)
                                        @if($program->id == $old_program)
                                        <option value="{{$program->id}}" selected>{{$program->name}}</option>
                                        @else
                                        <option value="{{$program->id}}">{{$program->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="form_studies" class="form-control-label">Форма обучения: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control content_direction" id="form_studies" data-href="{{ route('api_directions_get') }}">
                                        @foreach($form_studies as $form_study)
                                            <option value="{{$form_study->id}}">{{$form_study->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="level_studies" class="form-control-label">Уровень образования: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control content_direction" id="level_studies" data-href="{{ route('api_directions_get') }}">
                                        @foreach($level_studies as $level_study)
                                            <option value="{{$level_study->id}}">{{$level_study->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="faculty" class="form-control-label">Факультет: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control content_direction" id="faculty" data-href="{{ route('api_directions_get') }}">
                                        @foreach($faculties as $faculty)
                                            <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="direction" class="form-control-label">Направление подготовки: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control" id="direction" name="direction">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="year" class="form-control-label">Год: </label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" id="year" name="year" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="layout" class="form-control-label">Макет: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control content_direction" id="layout" name="layouts">
                                        @foreach($layouts as $layout)
                                            <option value="{{$layout->id}}">{{$layout->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="course" class="form-control-label">Курс: (введите только номера)</label>
                                </div>
                                <div class="col-lg-12 text">
                                    <input type="text" id="course" name="course" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label for="type" class="form-control-label">Тип дисциплины: </label>
                                </div>
                                <div class="col-lg-12">
                                    <select class="form-control content_direction" id="type" name="type">
                                        @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            <div class="form-actions form-group">
                                <div class="col-lg-4 push-lg-4">
                                    <input type="submit" value="Сохранить" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
