@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Преподаватели и дисциплины</div>

                    <div class="card-body">
                        <div class="btn-group" style="float: right;">
                            {{--<a href="{{ route('programs.create') }}" class=" btn btn-default">--}}
                                {{--Добавить программу--}}
                            {{--</a>--}}
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Дисциплина</th>
                                <th>Год</th>
                                {{--<th>Исполнитель</th>--}}
                                <th style="text-align: right;">Функционал</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($programs->count())
                                @foreach($programs as $program)
                                    <tr>
                                        <td>{{ $program->name }}</td>
                                        <td>{{ $program->directions->name }}</td>
                                        <td>{{ $program->year }}</td>
                                        <td style="text-align: right;">
                                            <a href="{{ route('programs.show', $section->id) }}" class="btn btn-primary">Просмотр</a>&nbsp; &nbsp;
                                            <a href="{{ route('programs.edit', $section->id) }}" class="btn btn-success">Редактирование</a>&nbsp; &nbsp;
                                            <a onclick="event.preventDefault(); document.getElementById('delete-product').submit();" href='#' class="btn btn-danger">Удаление</a>
                                            <form id="delete-product"
                                                  action="{{ route('programs.destroy', ['id' => $section->id]) }}"
                                                  method="POST"
                                                  style="display: none;"
                                            >
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
