<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramLayouts extends Model
{
    protected $fillable = ['program_id', 'section_id', 'description_value', 'first', 'check_done'];

    public function sections()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }

    public function programs()
    {
        return $this->belongsTo('App\Program', 'program_id');
    }
}
