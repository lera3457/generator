<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramType extends Model
{
    protected $fillable = ['name', 'short_name'];

    public function programs()
    {
        return $this->hasMany('App\Program', 'type_id');
    }
}
