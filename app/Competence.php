<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    protected $fillable = ['name', 'type_id', 'content', 'direction_id'];

    public function directions()
    {
        return $this->belongsTo('App\Direction', 'direction_id');
    }

    public function type()
    {
        return $this->belongsTo('App\TypeCompetence', 'type_id');
    }
}
