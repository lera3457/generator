<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    protected $fillable = ['name'];

    public function faculties()
    {
        return $this->belongsTo('App\Faculty', 'faculty_id');
    }

    public function level_studies()
    {
        return $this->belongsTo('App\LevelStudy', 'level_id');
    }

    public function form_studies()
    {
        return $this->belongsTo('App\FormStudy', 'form_study_id');
    }

    public function programs()
    {
        return $this->hasMany('App\Program', 'direction_id');
    }

    public function competences()
    {
        return $this->hasMany('App\Competence', 'direction_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
