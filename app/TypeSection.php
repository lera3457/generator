<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeSection extends Model
{
    protected $fillable = ['name', 'short_name', 'description'];
}
