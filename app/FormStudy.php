<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormStudy extends Model
{
    protected $fillable = ['name', 'short_name'];

    public function directions()
    {
        return $this->hasMany('App\Direction', 'form_study_id');
    }
}
