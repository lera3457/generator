<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelStudy extends Model
{
    protected $fillable = ['name', 'short_name'];

    public function directions()
    {
        return $this->hasMany('App\Direction', 'level_id');
    }
}
