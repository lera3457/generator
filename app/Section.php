<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'id', 'name', 'description', 'type_id'
    ];

    public function program_layouts()
    {
        return $this->hasMany('App\ProgramLayouts', 'section_id');
    }

    public function type()
    {
        return $this->belongsTo('App\TypeSection', 'type_id');
    }
}
