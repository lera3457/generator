<?php

namespace App\Http\Controllers;

use App\Competence;
use App\Direction;
use App\Faculty;
use App\FormStudy;
use App\LevelStudy;
use App\TypeSection;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function directions_get(Request $request)
    {
        $form_studies = $request->post('form_studies');
        $level_studies = $request->post('level_studies');
        $faculty = $request->post('faculty');
        $directions = Direction::where('form_study_id', $form_studies)->where('level_id', $level_studies)->where('faculty_id', $faculty)->get();

        return response()->json($directions);
    }

    public function directions_levelStudies(Request $request)
    {
        $directions = LevelStudy::find($request->post('id'))->directions()->get();
        return response()->json($directions);
    }

    public function directions_formStudies(Request $request)
    {
        $directions = FormStudy::find($request->post('id'))->directions()->get();
        return response()->json($directions);
    }

    public function competences_add(Request $request)
    {
        $name = $request->post('name');
        $type = $request->post('type');
        $direction = $request->post('direction');
        $content = $request->post('content');
        $flag = $request->post('flag');
        $id = $request->post('id');
        if($flag == 1 && $id){
            $competence = Competence::find($id);
            $competence->name = $name;
            $competence->type_id = $type;
            $competence->direction_id = $direction;
            $competence->content = $content;
            $competence->save();
        }
        else{
            $competence = Competence::create([
                'name' => $name,
                'type_id' => $type,
                'direction_id' => $direction,
                'content' => $content
            ]);
        }
        return response()->json($competence);
    }

    public function change_type_description(Request $request){
        $id = $request->post('id');
        if($id){
            $type = TypeSection::find($id);
        }
        else{
            $type = null;
        }
        return response()->json($type);

    }

    public function competence_get(Request $request){
        $id = $request->get('id');
        if($id){
            $competence = Competence::find($id);
        }
        else{
            $competence = null;
        }
        return response()->json($competence);
    }
}