<?php

namespace App\Http\Controllers;

use App\ProgramLayouts;
use Illuminate\Http\Request;

class ProgramLayoutsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProgramLayouts  $programLayouts
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramLayouts $programLayouts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProgramLayouts  $programLayouts
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramLayouts $programLayouts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgramLayouts  $programLayouts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramLayouts $programLayouts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProgramLayouts  $programLayouts
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramLayouts $programLayouts)
    {
        //
    }
}
