<?php

namespace App\Http\Controllers;

use App\Layout;
use App\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('ROLE_ADMIN')){
            return view('admin.layouts', ['layouts' => Layout::all()]);
        }
        else if(Auth::user()->hasRole('ROLE_TEACHER')){
            return view('admin.programs', ['programs' => Program::all()]);
        }
        return view('home');
    }
}
