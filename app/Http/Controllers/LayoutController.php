<?php

namespace App\Http\Controllers;

use App\Layout;
use App\Program;
use App\Section;
use Illuminate\Http\Request;

class LayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.layouts', ['layouts' => Layout::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::orderBy('name')->get();
        return view('admin.layout_add', ['sections' => $sections]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $m = 1;
        $n = $request->post('sections_numbers');
        $arr_section = [];
        while($m <= $n){
            $value = $request->post('section_'.$m);
            array_push($arr_section, $value);
            $m++;
        }
        $arr_section = json_encode($arr_section);

        $layout = Layout::create([
            'name' => $request->post('name'),
            'sections' => $arr_section,
            'year' => $request->post('year')
        ]);
        return redirect()->route('layouts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Layout  $layout
     * @return \Illuminate\Http\Response
     */
    public function show(Layout $layout)
    {
        $arr_sections = json_decode($layout->sections);
        $sections = [];
        foreach($arr_sections as $arr_section){
            $section_name = Section::find($arr_section)->description;
            array_push($sections, $section_name);
        }
        return view('admin.layout_show', ['layout' => $layout, 'sections' => $sections]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Layout  $layout
     * @return \Illuminate\Http\Response
     */
    public function edit(Layout $layout)
    {
        $sections_arr = Section::orderBy('name')->get();
        $arr_sections = json_decode($layout->sections);

        $sections_list = [];
        foreach($arr_sections as $arr_section){
            $section_name = Section::find($arr_section);
            array_push($sections_list, $section_name);
        }
        return view('admin.layout_edit', ['layout' => $layout, 'sections_list' => $sections_list, 'sections' => $sections_arr]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Layout  $layout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Layout $layout)
    {
        $m = 1;
        $n = $request->post('sections_numbers');
        echo $n;
        $arr_section = [];
        while($m <= $n){
            $value = $request->post('section_'.$m);
            array_push($arr_section, $value);
            $m++;
        }
        $arr_section = json_encode($arr_section);
        $layout->name = $request->post('name');
        $layout->sections = $arr_section;
        $layout->year = $request->post('year');
        $layout->save();
        return redirect()->route('layouts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Layout $layout
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Layout $layout)
    {
        $layout -> delete();
        return redirect()->route('layouts.index');
    }
}
