<?php

namespace App\Http\Controllers;

use App\Competence;
use App\Direction;
use App\Faculty;
use App\FormStudy;
use App\Layout;
use App\LevelStudy;
use App\Program;
use App\ProgramLayouts;
use App\ProgramType;
use App\Section;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.programs', ['programs' => Program::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int|null $id_program
     * @return \Illuminate\Http\Response
     */
    public function create($id_program = null)
    {
        $old_program = '';
        if($id_program != null){
            $old_program = $id_program;
        }
        $faculty = Faculty::orderBy('name')->get();
        $form_studies = FormStudy::orderBy('name')->get();
        $level_studies = LevelStudy::orderBy('name')->get();
        $layouts = Layout::orderBy("created_at", "DESC")->get();
        return view('admin.program_add', [
            'faculties' => $faculty, 'form_studies' => $form_studies,
            'level_studies' => $level_studies, 'layouts' => $layouts,
            'programs' => Program::all(), 'old_program' => $old_program,
            'types' => ProgramType::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $program = Program::create([
            'name' => $request->post('name'),
            'direction_id' => $request->post('direction'),
            'year' => $request->post('year'),
            'layout_id' => $request->post('layouts'),
            'user_id' => Auth::user()->id,
            'course' => $request->post('course'),
            'type_id' => $request->post('type')
        ]);

        $layout = Layout::find($request->post('layouts'));
        $arr_sections = json_decode($layout->sections);
        $n=0;
        $direction = Direction::find($request->post('direction'));
        $user = User::find(Auth::user()->id);
        foreach($arr_sections as $arr_section){
            $first = false;
            if($n == 0){
                $first = true;
            }
            $section = Section::find($arr_section);
            $context = $section->description;
            if(!empty($request->post('old_program'))){
                $old_program = ProgramLayouts::where('program_id', $request->post('old_program'))->where('section_id', $section->id)->first();
                $context = $old_program->description_value;
            }
            if(strpos($context, '#name_subject#') !== false){
                $context = preg_replace('/{#name_subject#}/', $request->post('name'), $context);
            }
            if(strpos($context, '{#name_direction#}') !== false){
                $context = preg_replace('/{#name_direction#}/', $direction->name, $context);
            }
            if(strpos($context, '{#name_short_direction#}') !== false){
                $context = preg_replace('/{#name_short_direction#}/', $direction->name, $context);
            }
            if(strpos($context, '{#level_study#}') !== false){
                $context = preg_replace('/{#level_study#}/', $direction->level_studies->short_name, $context);
            }
            if(strpos($context, '{#form_study#}') !== false){
                $context = preg_replace('/{#form_study#}/', $direction->form_studies->short_name, $context);
            }
            if(strpos($context, '{#year#}') !== false){
                $context = preg_replace('/{#year#}/', $request->post('year'), $context);
            }
            if(strpos($context, '{#fio_author#}') !== false){
                $fio = $user->surname;
                $context = preg_replace('/{#fio_author#}/', $fio, $context);
            }
            if(strpos($context, '{#course#}') !== false){
                $context = preg_replace('/{#course#}/', $request->post('course'), $context);
            }
            if(strpos($context, '{#type_2#}') !== false){
                $type = ProgramType::find($request->post('type'));
                $context = preg_replace('/{#type_2#}/', $type->short_name, $context);
            }

            $program_layouts = ProgramLayouts::create([
                'program_id' => $program->id,
                'section_id' => $section->id,
                'description_value' => $context,
                'first' => $first,
                'check_done' => false
            ]);
            $n++;
        }

        return redirect()->route('programs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        $layout = $program->layouts;
        $arr_sections = json_decode($layout->sections);
        $sections = [];
        foreach($arr_sections as $arr_section){
            $section_name = ProgramLayouts::where('program_id',$program->id)->where('section_id',$arr_section)->first()->description_value;
            array_push($sections, $section_name);
        }
        return view('admin.program_show', ['layout' => $program, 'sections' => $sections]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        $sections_arr = ProgramLayouts::where('program_id', $program->id)->get();
        $sections = [];
        foreach($sections_arr as $section_one){
            $section = Section::find($section_one->section_id);
            array_push($sections, $section);
        }

        $section_first_arr = ProgramLayouts::where('program_id', $program->id)->where('first', true)->first();
        $section_first = Section::find($section_first_arr->section_id);
//        $section_first = [];
//        foreach($section_first_arr as $section_f){
//            $section = Section::find($section_f->section_id);
//            array_push($section_first, $section);
//        }
        $comps = Competence::all();
        $section_first_value = ProgramLayouts::where('program_id', $program->id)->where('first', true)->first();
        return view('admin.program_edit', ['program' => $program, 'sections' => $sections,
            'section_first' => $section_first, 'section_first_value' => $section_first_value, 'comps' => $comps]);
    }

    public function edit_second($program_id, $section_id )
    {
        $program = Program::find($program_id);
        $sections_arr = ProgramLayouts::where('program_id', $program->id)->get();
        $sections = [];
        foreach($sections_arr as $section_one){
            $section = Section::find($section_one->section_id);
            array_push($sections, $section);
        }

//        $section_first_arr = ProgramLayouts::where('program_id', $program->id)->where('first', true)->first();
        $section_first = Section::find($section_id);
//        $section_first = [];
//        foreach($section_first_arr as $section_f){
//            $section = Section::find($section_f->section_id);
//            array_push($section_first, $section);
//        }
        $comps = Competence::all();
        $section_first_value = ProgramLayouts::where('program_id', $program->id)->where('section_id', $section_id)->first();
        return view('admin.program_edit', ['program' => $program, 'sections' => $sections, 'section_first' => $section_first, 'section_first_value' => $section_first_value, 'comps' => $comps]);
    }

    public function purpose_edit(Request $request){
        $program_id = $request->post('program_id');
        $section_id = $request->post('section_id');
        $purpose = $request->post('purpose');
        $tasks = $request->post('tasks');
        $section = ProgramLayouts::where('program_id', $program_id)->where('section_id', $section_id)->first();
        $context = $section->description_value;

        if(strpos($context, '#purpose#') !== false){
            $context = preg_replace('{#purpose#}', $purpose, $context);
        }
        if(strpos($context, '#tasks#') !== false){
            $context = preg_replace('{{#tasks#}}', $tasks, $context);
        }

        $section->description_value = $context;
        $section->save();
        return response()->json($section);
    }

    public function place_change(Request $request){
        $program_id = $request->post('program_id');
        $section_id = $request->post('section_id');
        $knowledge_m = $request->post('knowledge_m');
        $skill_m = $request->post('skill_m');
        $attainment_m = $request->post('attainment_m');
        $previous_discipline = $request->post('previous_discipline');
        $future_discipline = $request->post('future_discipline');
        $section = ProgramLayouts::where('program_id', $program_id)->where('section_id', $section_id)->first();
        $context = $section->description_value;

        if(strpos($context, '#knowledge_m#') !== false){
            $context = preg_replace('{{#knowledge_m#}}', $knowledge_m, $context);
        }
        if(strpos($context, '#skill_m#') !== false){
            $context = preg_replace('{{#skill_m#}}', $skill_m, $context);
        }
        if(strpos($context, '#attainment_m#') !== false){
            $context = preg_replace('{{#attainment_m#}}', $attainment_m, $context);
        }
        if(strpos($context, '#previous_discipline#') !== false){
            $context = preg_replace('{{#previous_discipline#}}', $previous_discipline, $context);
        }
        if(strpos($context, '#future_discipline#') !== false){
            $context = preg_replace('{{#future_discipline#}}', $future_discipline, $context);
        }

        $section->description_value = $context;
        $section->save();
        return response()->json($section);
    }

    public function hours_change(Request $request){
        $program_id = $request->post('program_id');
        $section_id = $request->post('section_id');
        $gk = $request->post('gk');
        $ik = $request->post('ik');
        $ai = $request->post('ai');
        $l_09 = $request->post('l_09');
        $pz = $request->post('pz');
        $lr = $request->post('lr');
        $sr = $request->post('sr');
        $section = ProgramLayouts::where('program_id', $program_id)->where('section_id', $section_id)->first();
        $context = $section->description_value;

        if(strpos($context, '#gk_workProg#') !== false){
            $context = preg_replace('{{#gk_workProg#}}', $gk, $context);
        }
        if(strpos($context, '#ik_workProg#') !== false){
            $context = preg_replace('{{#ik_workProg#}}', $ik, $context);
        }
        if(strpos($context, '#ai_workProg#') !== false){
            $context = preg_replace('{{#ai_workProg#}}', $ai, $context);
        }
        if(strpos($context, '#l_workProg#') !== false){
            $context = preg_replace('{{#l_workProg#}}', $l_09, $context);
        }
        if(strpos($context, '#pz_workProg#') !== false){
            $context = preg_replace('{{#pz_workProg#}}', $pz, $context);
        }
        if(strpos($context, '#lr_workProg#') !== false){
            $context = preg_replace('{{#lr_workProg#}}', $lr, $context);
        }
        if(strpos($context, '#sr_workProg#') !== false){
            $context = preg_replace('{{#sr_workProg#}}', $sr, $context);
        }

        $section->description_value = $context;
        $section->save();
        return response()->json($section);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgramLayouts  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramLayouts $program)
    {
        $program->description_value = $request->post('text');
        $program->save();
        return redirect()->route('programs.index');
    }

    public function update_second(Request $request, $program)
    {
        $pr_l = ProgramLayouts::find($program);
        $pr_l->description_value = $request->post('text');
        $pr_l->save();
        return Redirect::back()->with('message','Operation Successful !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Program $program)
    {
        $program -> delete();
        return redirect()->route('programs.index');
    }
}
