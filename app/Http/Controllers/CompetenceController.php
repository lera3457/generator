<?php

namespace App\Http\Controllers;

use App\Competence;
use App\Direction;
use App\TypeCompetence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competences = DB::table('competences')
            ->groupBy('direction_id')
            ->get();
        $competences = DB::table('competences')
            ->select("competences.id as id", "directions.name as directions_name", "directions.id as directions_id" )
            ->join("directions", "competences.direction_id", "=", "directions.id")
            ->groupBy('competences.direction_id')
            ->get();
        return view('admin.competences', ['competences' => $competences]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $directions = Direction::orderBy('name')->get();
        $types = TypeCompetence::all();
        return view('admin.competence_add', [
            'directions' => $directions,
            'types' => $types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return view('admin.competences', ['competences' => Competence::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param Competence $competence
     * @return \Illuminate\Http\Response
     * @internal param Competence|Direction $competence
     */
    public function show(Competence $competence)
    {
        $direction = Direction::find($competence->direction_id);
        $competences = Competence::where('direction_id', $direction->id)->get();
        return view('admin.competence_show', ['competences' => $competences, 'direction' => $direction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function edit(Competence $competence)
    {
        $types = TypeCompetence::all();
        $direction = Direction::find($competence->direction_id);
        $competences = Competence::where('direction_id', $direction->id)->get();
        return view('admin.competence_edit', ['competences' => $competences, 'direction' => $direction, 'types' => $types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competence $competence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Competence $competence)
    {
        //
    }
}
