<?php

namespace App\Http\Controllers;

use App\Section;
use App\TypeSection;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sections', ['sections' => Section::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = TypeSection::all();
        return view('admin.section_add', ['types' => $types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $section = Section::create([
            'name' => $request->post('name'),
            'description' => $request->post('text'),
            'type_id' => $request->post('type')
            ]);
        return redirect()->route('sections.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        $types = TypeSection::all();
        return view('admin.section_show', ['section' => $section, 'types' => $types]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        $types = TypeSection::all();
        return view('admin.section_edit', ['section' => $section, 'types' => $types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        $section->name = $request->post('name');
        $section->description = $request->post('text');
        $section->type_id = $request->post('type');
        $section->save();
        return redirect()->route('sections.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Section $section
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Section $section)
    {
        $section -> delete();
        return redirect()->route('sections.index');
    }
}
