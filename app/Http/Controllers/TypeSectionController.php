<?php

namespace App\Http\Controllers;

use App\TypeSection;
use Illuminate\Http\Request;

class TypeSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeSection  $typeSection
     * @return \Illuminate\Http\Response
     */
    public function show(TypeSection $typeSection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeSection  $typeSection
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeSection $typeSection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeSection  $typeSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeSection $typeSection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeSection  $typeSection
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeSection $typeSection)
    {
        //
    }
}
