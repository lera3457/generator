<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCompetence extends Model
{
    protected $fillable = ['name', 'full_name'];
}
