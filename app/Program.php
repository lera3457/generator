<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = ['name', 'year', 'layout_id', 'user_id', 'direction_id', 'course', 'type_id'];

    public function directions()
    {
        return $this->belongsTo('App\Direction', 'direction_id');
    }
    public function types()
    {
        return $this->belongsTo('App\ProgramType', 'type_id');
    }
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function program_layouts()
    {
        return $this->hasMany('App\ProgramLayouts', 'program_id');
    }
    public function layouts()
    {
        return $this->belongsTo('App\Layout', 'layout_id');
    }
}
