<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    protected $fillable = ['name', 'sections', 'year', 'created_at'];

    public function programs()
    {
        return $this->hasMany('App\Program', 'layout_id');
    }
}
