<?php

use App\TypeCompetence;
use Illuminate\Database\Seeder;

class TypeCompetenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new TypeCompetence();
        $level->name = 'ОК';
        $level->full_name = 'общекультурные (ОК)';
        $level->save();

        $level1 = new TypeCompetence();
        $level1->name = 'ОПК';
        $level1->full_name = 'общепрофессиональные (ОПК)';
        $level1->save();

        $level2 = new TypeCompetence();
        $level2->name = 'ПК';
        $level2->full_name = 'профессиональные (ПК)';
        $level2->save();
    }
}
