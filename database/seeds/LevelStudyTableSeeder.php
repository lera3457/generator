<?php

use App\levelStudy;
use Illuminate\Database\Seeder;

class LevelStudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new LevelStudy();
        $level->name = 'Бакалавриат';
        $level->short_name = 'бакалавр';
        $level->save();

        $level1 = new LevelStudy();
        $level1->name = 'Специалитет';
        $level1->short_name = 'специалист';
        $level1->save();

        $level2 = new LevelStudy();
        $level2->name = 'Магистратура';
        $level2->short_name = 'магистр';
        $level2->save();

        $level3 = new LevelStudy();
        $level3->name = 'СПО';
        $level3->short_name = 'СПО';
        $level3->save();
    }
}
