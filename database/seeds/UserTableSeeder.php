<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'ROLE_ADMIN')->first();
        $role_teacher  = Role::where('name', 'ROLE_TEACHER')->first();

        $employee = new User();
        $employee->name = 'Админ';
        $employee->surname = 'Иванова';
        $employee->email = 'admin@mail.ru';
        $employee->password = bcrypt('admin');
        $employee->save();
        $employee->roles()->attach($role_admin);

        $manager = new User();
        $manager->name = 'Анна';
        $manager->surname = 'Иванова';
        $manager->email = 'teacher@mail.ru';
        $manager->password = bcrypt('teacher');
        $manager->save();
        $manager->roles()->attach($role_teacher);
    }
}
