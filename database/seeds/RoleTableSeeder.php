<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'ROLE_ADMIN';
        $role_employee->description = 'Админ';
        $role_employee->save();

        $role_manager = new Role();
        $role_manager->name = 'ROLE_TEACHER';
        $role_manager->description = 'Преподаватель';
        $role_manager->save();
    }
}
