<?php

use App\ProgramType;
use Illuminate\Database\Seeder;

class ProgramTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new ProgramType();
        $level->name = 'Базовая часть';
        $level->short_name = 'базовой части';
        $level->save();

        $level1 = new ProgramType();
        $level1->name = 'Вариативная часть';
        $level1->short_name = 'вариативной части';
        $level1->save();

        $level2 = new ProgramType();
        $level2->name = 'Дисциплина по выбору';
        $level2->short_name = 'дисциплине по выбору';
        $level2->save();
    }
}
