<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Role comes before User seeder here.
        $this->call(RoleTableSeeder::class);
        // User seeder will use the roles above created.
        $this->call(UserTableSeeder::class);
        $this->call(FacultyTableSeeder::class);
        $this->call(FormStudyTableSeeder::class);
        $this->call(LevelStudyTableSeeder::class);
        $this->call(DirectionTableSeeder::class);
        $this->call(ProgramTypesTableSeeder::class);
        $this->call(TypeSectionTableSeeder::class);
        $this->call(TypeCompetenceTableSeeder::class);
    }
}
