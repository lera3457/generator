<?php

use App\Faculty;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new Faculty();
        $level->name = 'Биологический факультет';
        $level->save();

        $level = new Faculty();
        $level->name = 'Геолого-географический факультет';
        $level->save();

        $level = new Faculty();
        $level->name = 'Исторический факультет';
        $level->save();

        $level = new Faculty();
        $level->name = 'Факультет архитектуры и дизайна';
        $level->save();

        $level = new Faculty();
        $level->name = 'Факультет математики и информационных технологий';
        $level->save();

        $level = new Faculty();
        $level->name = 'Физико-технический факультет';
        $level->save();

        $level = new Faculty();
        $level->name = 'Факультет иностранных языков';
        $level->save();
    }
}
