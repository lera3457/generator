<?php

use App\FormStudy;
use Illuminate\Database\Seeder;

class FormStudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new FormStudy();
        $level->name = 'Очная';
        $level->short_name = 'очная';
        $level->save();

        $level1 = new FormStudy();
        $level1->name = 'Заочная';
        $level1->short_name = 'заочная';
        $level1->save();

        $level2 = new FormStudy();
        $level2->name = 'Очно-заочная';
        $level2->short_name = 'очно-заочная';
        $level2->save();

    }
}
