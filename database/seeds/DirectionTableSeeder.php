<?php

use App\Direction;
use App\Faculty;
use App\FormStudy;
use App\LevelStudy;
use Illuminate\Database\Seeder;

class DirectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facul_math  = Faculty::where('name', 'Факультет математики и информационных технологий')->first()->id;
        $facul_bio  = Faculty::where('name', 'Биологический факультет')->first()->id;

        $ochn  = FormStudy::where('name', 'Очная')->first()->id;
        $ochn_zao  = FormStudy::where('name', 'Очно-заочная')->first()->id;

        $level_id  = LevelStudy::where('name', 'Бакалавриат')->first()->id;
        $level_mag  = LevelStudy::where('name', 'Магистратура')->first()->id;

        $employee = new Direction();
        $employee->name = 'Прикладная математика';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '06.03.01. Биология. Профиль "Физиология человека и животных"';
        $employee->faculty_id = $facul_bio;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '06.03.01. Биология. Профиль "Экология"';
        $employee->faculty_id = $facul_bio;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '06.03.02. Почвоведение. Профиль "Земельный кадастр и сертификация почв"';
        $employee->faculty_id = $facul_bio;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '06.03.01. Биология. Профиль "Экология"';
        $employee->faculty_id = $facul_bio;
        $employee->form_study_id = $ochn_zao;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '21.03.02. Землеустройство и кадастры. Профиль "Земельный кадастр"';
        $employee->faculty_id = $facul_bio;
        $employee->form_study_id = $ochn_zao;
        $employee->level_id = $level_id;
        $employee->save();

//        fmiit
        $employee = new Direction();
        $employee->name = '01.03.01. Математика';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '01.03.02. Прикладная математика и информатика';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '09.03.01. Информатика и вычислительная техника';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '09.03.02. Информационные системы и технологии';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '10.03.01. Информационная безопасность';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '12.03.04. Биотехнические системы и технологии';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '27.03.02. Управление качеством';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_id;
        $employee->save();

        $employee = new Direction();
        $employee->name = '09.04.02. Информационные системы и технологии. Программа "Прикладные информационные технологии"';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_mag;
        $employee->save();

        $employee = new Direction();
        $employee->name = '09.04.02. Информационные системы и технологии. Программа"Управление данными (по отраслям)"';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_mag;
        $employee->save();

        $employee = new Direction();
        $employee->name = '44.04.01. Педагогическое образование. Программа "Информатика, информационные технологии в образовании"';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_mag;
        $employee->save();

        $employee = new Direction();
        $employee->name = '44.04.01. Педагогическое образование. Программа "Математическое образование"';
        $employee->faculty_id = $facul_math;
        $employee->form_study_id = $ochn;
        $employee->level_id = $level_mag;
        $employee->save();
    }
}
