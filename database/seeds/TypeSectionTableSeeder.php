<?php

use App\TypeSection;
use Illuminate\Database\Seeder;

class TypeSectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = new TypeSection();
        $level->name = 'Структура и содержание дисциплины';
        $level->short_name = 'hours';
        $level->description = 'Используйте этот тип шаблона для раздела, который содержит таблицу с указанием количества академических или астрономических часов, выделенных на контактную работу обучающихся с преподавателем (по видам учебных занятий) и на самостоятельную работу обучающихся.';
        $level->save();

        $level1 = new TypeSection();
        $level1->name = 'Учебно-методическое и информационное обеспечение дисциплины';
        $level1->short_name = 'library';
        $level1->description = 'Используйте этот тип шаблона для раздела, который содержит список используемых источников для дисциплины.';
        $level1->save();

        $level2 = new TypeSection();
        $level2->name = 'Цели и задачи';
        $level2->short_name = 'tasks';
        $level2->description = 'Используйте этот тип шаблона для раздела, который содержит цель и список задач для дисциплины.';
        $level2->save();

        $level3 = new TypeSection();
        $level3->name = 'Место дисциплины в структуре ОПОП';
        $level3->short_name = 'data_direction';
        $level3->description = 'Используйте этот тип шаблона для раздела, который содержит умения, знания, навыки, а также информацию о предыдущих и последующих дисциплинах.';
        $level3->save();

        $level4 = new TypeSection();
        $level4->name = 'Компетенции обучающегося';
        $level4->short_name = 'competence';
        $level4->description = 'Используйте этот тип шаблона для раздела, который содержит список компетенций обучающегося.';
        $level4->save();
    }
}
