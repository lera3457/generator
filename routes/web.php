<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('sections', 'SectionController');
Route::resource('programs', 'ProgramController');
Route::resource('competences', 'CompetenceController');
Route::post('/competences/add_element/', 'ApiController@competences_add')->name('competences_add');
Route::get('/programs/create/{id_program?}', 'ProgramController@create')->name('create_programs');
Route::resource('layouts', 'LayoutController');
Route::resource('teachers', 'TeacherController');
Route::resource('directions', 'DirectionController');
Route::post('/api/directions/', 'ApiController@directions_get')->name('api_directions_get');
Route::get('/api/sections/', 'ApiController@change_type_description')->name('api_change_type_description');
Route::get('/api/competence_get/', 'ApiController@competence_get')->name('competence_get');
Route::get('/programs/{id_pr}/edit/{id_sec}', 'ProgramController@edit_second')->name('edit_sections');
Route::put('/programs/update/{id_pl}', 'ProgramController@update_second')->name('update_second');
Route::post('/api/purpose_edit/', 'ProgramController@purpose_edit')->name('purpose_edit');
Route::post('/api/place_change/', 'ProgramController@place_change')->name('place_change');
Route::post('/api/hours_change/', 'ProgramController@hours_change')->name('hours_change');
//Route::post('/api/directions/levelStudies', 'ApiController@directions_levelStudies')->name('api_directions_levelStudies');
//Route::post('/api/directions/formStudies', 'ApiController@directions_formStudies')->name('api_directions_formStudies');
