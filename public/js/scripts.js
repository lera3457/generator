$(document).ready(function() {
    var name_subject  = '<span style="font-family: \'Times New Roman\', serif;"><span style="font-size: 18.6667px;"><strong>{#name_subject#}</strong></span></span>';
    var fio_author  = '<strong><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#fio_author#}</span></strong>';
    var name_direction  = '<span style="font-family: \'Times New Roman\', serif;"><span style="font-size: 16px;"><strong>{#name_direction#}</strong></span></span>';
    var name_short_direction = '<strong style="font-family: \'Times New Roman\', serif; font-size: 16px;">{#name_short_direction#}</strong>';
    var level_study  = '<span style="font-family: \'Times New Roman\', serif;"><span style="font-size: 16px;"><strong>{#level_study#}</strong></span></span>';
    var form_study  = '<strong><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#form_study#}</span></strong>';
    var year  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#year#}</span>';
    var course = '<strong><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#course#}</span></strong>';
    var type_program  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#type_1#}</span>';
    var type_program_2  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#type_2#}</span>';
    var purpose  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#purpose#}</span>';
    var tasks  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#tasks#}</span>';
    var previous_discipline  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#previous_discipline#}</span>';
    var future_discipline  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#future_discipline#}</span>';
    var knowledge_m  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#knowledge_m#}</span>';
    var skill_m  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#skill_m#}</span>';
    var attainment_m = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#attainment_m#}</span>';

    var l_workProg  = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#l_workProg#}</span>';
    var pz_workProg  = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#pz_workProg#}</span>';
    var lr_workProg  = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#lr_workProg#}</span>';
    var gk_workProg  = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#gk_workProg#}</span>';
    var ik_workProg  = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#ik_workProg#}</span>';
    var ai_workProg  = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#ai_workProg#}</span>';
    var sr_workProg = '<span style="font-size: 11.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#sr_workProg#}</span>';

    var main_literature  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#main_literature#}</span>';
    var dop_literature  = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#dop_literature#}</span>';
    var web_literature = '<span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; mso-fareast-font-family: \'Times New Roman\'; mso-fareast-language: RU;">{#web_literature#}</span>';
    tinymce.init({
        selector:'#mytextarea',
        resize: false,
        language: 'ru',
        templates: [
            {
                title: "Наименование дисциплины",
                description: "Вставьте данный шаблон в место, где должно быть наименование дисциплины. ",
                content: name_subject
            },
            {
                title: "Составитель(-и)",
                description: "Вставьте данный шаблон в место, где должны быть ФИО составителей.",
                content: fio_author
            },
            {
                title: "Направление подготовки / специальность",
                description: "Вставьте данный шаблон в место, где должно быть направление подготовки.",
                content: name_direction
            },
            {
                title: "Направленность (профиль) ОПОП",
                description: "Вставьте данный шаблон в место, где должна быть направленность (профиль) ОПОП.",
                content: name_short_direction
            },
            {
                title: "Квалификация (степень)",
                description: "Вставьте данный шаблон в место, где должна быть квалификация.",
                content: level_study
            },
            {
                title: "Форма обучения",
                description: "Вставьте данный шаблон в место, где должна быть форма обучения.",
                content: form_study
            },
            {
                title: "Год приема",
                description: "Вставьте данный шаблон в место, где должен быть год приема.",
                content: year
            },
            {
                title: "Курс",
                description: "Вставьте данный шаблон в место, где должжен быть курс.",
                content: course
            },
            {
                title: "Тип дисциплины",
                description: "Вставьте данный шаблон в место, где должжен быть курс.",
                content: type_program
            },
            {
                title: "Тип дисциплины в род.п.",
                description: "Вставьте данный шаблон в место, где должжен быть курс.",
                content: type_program_2
            },
            {
                title: "Цель",
                description: "Вставьте данный шаблон в место, где должна быть цель дисциплины.",
                content: purpose
            },
            {
                title: "Задачи",
                description: "Вставьте данный шаблон в место, где должны быть задачи дисциплины.",
                content: tasks
            },
            {
                title: "Предшествующие дисциплины",
                description: "Вставьте данный шаблон в место, где должны быть предшествующие дисциплины.",
                content: previous_discipline
            },
            {
                title: "Последующие дисциплины",
                description: "Вставьте данный шаблон в место, где должны быть последующие дисциплины.",
                content: future_discipline
            },
            {
                title: "Знания",
                description: "Вставьте данный шаблон в место, где должны быть знания дисциплины.",
                content: knowledge_m
            },
            {
                title: "Умения",
                description: "Вставьте данный шаблон в место, где должны быть умения дисциплины.",
                content: skill_m
            },
            {
                title: "Навыки",
                description: "Вставьте данный шаблон в место, где должны быть навыки дисциплины.",
                content: attainment_m
            },
            {
                title: "Лекции, ч",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для лекций дисциплины.",
                content: l_workProg
            },
            {
                title: "Практические занятия, ч.",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для практических занятий дисциплины.",
                content: pz_workProg
            },
            {
                title: "Лабораторные работы, ч.",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для лабораторных работ дисциплины.",
                content: lr_workProg
            },
            {
                title: "Групповые консультации, ч.",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для групповыч консультаций дисциплины.",
                content: gk_workProg
            },
            {
                title: "Индивидуальные консультации и иные учебные занятия, ч.",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для индивидуальных консультации и иных учебных занятий дисциплины.",
                content: ik_workProg
            },
            {
                title: "Аттестационные испытания промежуточной аттестации обучающихся, ч.",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для аттестационных испытаний промежуточной аттестации обучающихся дисциплины.",
                content: ai_workProg
            },
            {
                title: "Самостоятельная работа, ч",
                description: "Вставьте данный шаблон в место, где должны быть подсчитаны часы для самостоятельной работы дисциплины.",
                content: sr_workProg
            },
            {
                title: "Основная литература",
                description: "Вставьте данный шаблон в место, где должен быть список основной литературы дисциплины.",
                content: main_literature
            },
            {
                title: "Дополнительная литература",
                description: "Вставьте данный шаблон в место, где должен быть список дополнительной литературы дисциплины.",
                content: dop_literature
            },
            {
                title: "Перечень ресурсов информационно-телекоммуникационной сети «Интернет»",
                description: "Вставьте данный шаблон в место, где должен быть список ресурсов информационно-телекоммуникационной сети «Интернет».",
                content: web_literature
            }
            ],

        powerpaste_allow_local_images: true,
        plugins: [
            " autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link  media noneditable preview print",
            " searchreplace table template textcolor visualblocks wordcount"
        ],
        toolbar:
            "a11ycheck undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | fontselect fontsizeselect | template print"
    });


    if ($("#id_sec").length > 0){
        var val = $("#id_sec").val();
        $("#" + val + "_sec").addClass('active');
    }

    var select_direction = $('#direction');
    select_direction.prop('disabled', true);
    $(".content_direction").change(function () {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: $(this).data('href'),
            data:{
                'form_studies': $('#form_studies').val(),
                'level_studies': $('#level_studies').val(),
                'faculty': $('#faculty').val()
            },
            success: function (response) {
                if(response.length>0){
                    select_direction.find("option").remove();
                    select_direction.prop('disabled', false);
                    $("#direction option").remove();
                    $.each(response, function(key, value) {
                        select_direction
                            .append($("<option></option>")
                                .attr("value",value['id'])
                                .text(value['name']));
                    });
                }
                else{
                    select_direction.find("option").remove();
                    select_direction.prop("disabled", true);
                }

                console.log(response);
            },
            error: function (e) {
                console.error(e);
            }
        });
    });
    $("#add_section").click(function () {
        var number = $(this).data('num');
        var elem = document.createElement('tr');
        var sections = $(this).data('sections');
        var options = '';
        $.each( sections, function(k, v) {
            var option = '<option value="' + v.id + '">' + v.name + '</option>';
            options += option;
        });
        elem.innerHTML = '<td>' + number + '</td>' +
            '<td><select class="form-control select_sections" onchange="select_cont(this.value, ' + number +')" name="section_' + number + '">' +
            '<option value="">Выберите раздел</option>' + options +
            '</select></td>';
        var last_tr = document.getElementById("last_tr");
        var parentDiv = last_tr.parentNode;
        parentDiv.insertBefore(elem, last_tr);
        $(this).data('num', number+1);
    });
    $("#add_competence").click(function () {
        var flag = $(this).data('edit');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: $(this).data('href'),
            data:{
                'name': $('#name').val(),
                'type': $('#type').val(),
                'direction': $('#directions').val(),
                'content': $('#content').val(),
                'flag': $(this).data('edit'),
                'id': $('#id_edit').val()
            },
            success: function (response) {
                var elem = '';
                if(!flag){
                    elem = '<div class="card">\n' +
                        '<div class="card-header" id="heading_' + response.id + '">\n' +
                        '<h5 class="mb-0">\n' +
                        '<a style="color: #3490dc" class="btn btn-link collapsed"\n' +
                        'data-toggle="collapse" data-target="#collapse' + response.id + '"\n' +
                        'aria-expanded="false" aria-controls="collapse' + response.id + '"\n' +
                        'role="button">' + response.name + '</a>\n' +
                        '</h5>\n' +
                        '</div>\n' +
                        '<div id="collapse' + response.id + '" class="collapse" aria-labelledby="heading' + response.id + '"\n' +
                        'data-parent="#accordion">\n' +
                        '<div class="card-body">' + response.content + '</div>\n' +
                        '</div>\n' +
                        '</div>';
                }
                else{
                    elem = '<div class="card">\n' +
                        '<a style="color: #3490dc" class="btn btn-link competence_change"\n' +
                        'data-id=' + response.id + '\n' +
                        'data-href="{{ route(\'competence_get\') }}"' +
                        'role="button">' + response.name + '</a>\n' +
                        '</div>';
                }
                var type = '';
                var accordion = '';
                if(response.type_id == '1'){
                    type = $('#competence_op');
                    accordion = $('#competence_op #accordion');
                }
                else if(response.type_id == '2'){
                    type = $('#competence_opk');
                    accordion = $('#competence_opk #accordion');
                }
                else if(response.type_id == '3'){
                    type = $('#competence_pk');
                    accordion = $('#competence_pk #accordion');
                }
                accordion.append(elem).fadeIn(500);
                type.fadeIn(500);
                $('#directions').prop('disabled', 'disabled');
            },
            error: function (e) {
                console.error(e);
            }
        });
    });
    $("#time_equals").click(function () {
        var z = $('#ball').val();
        var gk = $('#gk').val();
        var ik = $('#ik').val();
        var ai = $('#ai').val();
        var l_09 = $('#l_09').val();
        var pz = $('#pz').val();
        var lr = $('#lr').val();
        var sr = $('#sr').val();
        if(z == "" || gk == "" || ik == "" || ai == "" || l_09 == "" || pz == "" || lr == "" || sr == ""){
            alert("Пожалуйста заполните все поля.");
        }
        else{
            var sum1 = gk*1+ai*1+ik*1;
            var sum2 = 2*z;
            var sum12 = l_09*1 + pz*1 + lr*1 + sum1;
            var sum3 = z*36-sum12;
            if(sum1 != sum2){
                alert("Не соблюдено выражение: ГК + АИ + ИК = 2*зачетные единицы");
            }
            else if(sr != sum3){
                alert("Не соблюдено выражение: СР = (зачетные единицы) * 36 - Л - ЛР - ПЗ - ГК - ИК – АИ");
            }
            else{
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    url: '/api/hours_change',
                    data:{
                        'section_id': $(this).data('id'),
                        'program_id': $(this).data('prog_id'),
                        'gk': gk,
                        'ik': ik,
                        'ai': ai,
                        'l_09': l_09,
                        'pz': pz,
                        'lr': lr,
                        'sr': sr
                    },
                    success: function (response) {
                        location.reload();
                    },
                    error: function (e) {
                        console.error(e);
                    }
                });

            }
        }
    });
    $("#section_1").click(function () {
        var sec_1 = $('#sec_1').val();
        var sec_2 = $('#sec_2').val();
    });
    $("#class_lit").change(function () {
        var type = $('#class_lit').val();
        var div = document.getElementsByClassName("lit_book");
        if(type == 2){
            for(var i = 0; i < div.length; i++){
                div[i].style.display = "none";
            }
        }
        else{
            for(var i = 0; i < div.length; i++){
                div[i].style.display = "block";
            }
        }
    });
    $("#section_2").click(function () {
        var class_lit = $('#class_lit').val();
        var name_lit = $('#name_lit').val();
        var url_lit = $('#url_lit').val();
        var string = '';

        if(class_lit == 2){
            var num = $( "#type_2" ).data( "num" ) + 1;
            string = name_lit + ' - ' + url_lit;
            $('#type_2').append("<ol>" + num + '. ' + string + "</ol>");
            $( "#type_2" ).data( "num",  num);
            $( "#type_2" ).parent().css("display", "block");
        }
        else{
            var author_lit = $('#author_lit').val();
            var izd_lit = $('#izd_lit').val();
            var year_lit = $('#year_lit').val();
            var page_lit = $('#page_lit').val();
            string = author_lit.split(',')[0] + " " + name_lit + " / " + author_lit + ' - ' + izd_lit + ', ' + year_lit + ' - ' + page_lit + 'c. ' + url_lit;
            if(class_lit == 0){

                var num = $( "#type_0" ).data( "num" ) + 1;
                $('#type_0').append("<ol>" + num + '. ' + string + "</ol>");
                $( "#type_0" ).data( "num",  num);
                $( "#type_0" ).parent().css("display", "block");
            }
            else{
                var num = $( "#type_1" ).data( "num" ) + 1;
                $('#type_1').append("<ol>" + num + '. ' + string + "</ol>");
                $( "#type_1" ).data( "num",  num);
                $( "#type_1" ).parent().css("display", "block");
            }

        }


    });
    $(document).on('click', '.competence_change', function() {
        $.ajax({
            method: 'GET',
            url: '/api/competence_get',
            data:{
                'id': $(this).data('id')
            },
            success: function (response) {
                if(response.id){
                    $("#name").val(response.name);
                    $("#content").val(response.content);
                    $("#type").val(response.type_id);
                    $("#id_edit").val(response.id);
                    $("#add_competence").text("Изменить");
                }
            },
            error: function (e) {
                console.error(e);
            }
        });
    });
    $("#description_type").change(function () {
        var type = $('#description_type').val();
        $.ajax({
            method: 'GET',
            url: $(this).data('href'),
            data:{
                'id': type
            },
            success: function (response) {
               if(response.id){
                   $('#text_description').fadeOut(200);
                   $('#description_text_type').text(response.description);
                   $('#text_description').fadeIn(300);
               }
               else{
                   $('#text_description').hide(300);
               }
            },
            error: function (e) {
                console.error(e);
            }
        });
    });
    $("#purpose_submit").click(function () {
        var purpose = $('#purpose').val();
        var tasks = $('#tasks').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: '/api/purpose_edit',
            data:{
                'section_id': $(this).data('id'),
                'program_id': $(this).data('prog_id'),
                'purpose': purpose,
                'tasks': tasks
            },
            success: function (response) {
                location.reload();
            },
            error: function (e) {
                console.error(e);
            }
        });
    });
    $("#place_change").click(function () {
        var knowledge_m = $('#knowledge_m').val();
        var skill_m = $('#skill_m').val();
        var attainment_m = $('#attainment_m').val();
        var previous_discipline = $('#previous_discipline').val();
        var future_discipline = $('#future_discipline').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: '/api/place_change',
            data:{
                'section_id': $(this).data('id'),
                'program_id': $(this).data('prog_id'),
                'knowledge_m': knowledge_m,
                'skill_m': skill_m,
                'attainment_m': attainment_m,
                'previous_discipline': previous_discipline,
                'future_discipline': future_discipline
            },
            success: function (response) {
                location.reload();
            },
            error: function (e) {
                console.error(e);
            }
        });
    });
});
function select_cont(value, num) {
   /* var arrSection = {};
    var new_value_obj = {
        'number': num,
        'value_id': value
    };*/
    /*var hidden = $('#section_cont').val();
    console.log(hidden);
    if(!hidden){
        arrSection = new_value_obj;
    }
    else{
        var n = hidden.value.length;
        new_value_obj[n-1] = hidden;
        arrSection = new_value_obj;
    }
    // arrSection.push(value);
    console.log(arrSection);*/
    $('#section_cont').val($('#context_sections .select_sections').length);
}

